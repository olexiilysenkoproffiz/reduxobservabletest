import React from 'react';
import { connect } from 'react-redux';

import createSensorDataAction from '../actions/SensorData';

import HumidityEventEmitter, { humidity } from '../eventEmitter/HumidityEventEmitter'
import AirPressureEventEmitter, { airPressure } from '../eventEmitter/AirPressureEventEmitter'
import TemperatureEventEmitter, { temperature } from '../eventEmitter/TemperatureEventEmitter'

class Dashboard extends React.Component {
  constructor() {
    super();

    this.airPressureTimerID = this.runTimer(airPressure);
    this.temperatureTimerID = this.runTimer(temperature);
    this.humidityTimerID = this.runTimer(humidity);

    AirPressureEventEmitter.on(airPressure, (data) => {
      this.props.processSensorData(airPressure, data);
      clearTimeout(this.airPressureTimerID)
      this.airPressureTimerID = this.runTimer(airPressure);
    });

    HumidityEventEmitter.on(humidity, (data) => {
      this.props.processSensorData(humidity, data)
      clearTimeout(this.humidityTimerID)
      this.humidityTimerID = this.runTimer(humidity);
    });

    TemperatureEventEmitter.on(temperature, (data) => {
      this.props.processSensorData(temperature, data)
      clearTimeout(this.temperatureTimerID)
      this.temperatureTimerID = this.runTimer(temperature);
    });
  }

  runTimer(actionType) {
    return setTimeout(() => {
      this.props.processSensorData(actionType, "N/A")
    }, 1000);
  }

  render() {
    return (
      <div>
        <div style={paramStyle}>Humidity: <span style={{ marginLeft: '300px', color: 'blue', fontSize: '30px' }}>{this.props.displayObject.humidity}</span></div>
        <div style={paramStyle}>AirPressure: <span style={{ marginLeft: '260px', color: 'blue', fontSize: '30px' }}>{this.props.displayObject.airPressure}</span></div>
        <div style={paramStyle}>Temperature: <span style={valueStyle}>{this.props.displayObject.temperature}</span></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    displayObject: state.sourceDataReducer
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    processSensorData: (type, data) => {
      dispatch(createSensorDataAction(type, data))
    },
  }
}

const paramStyle = {
  border: '1px solid grey',
  width: '500px',
  height: '50px',
  textAlign: 'start',
  background: 'lightgrey',
  paddingLeft: '20px',
  paddingTop: '30px',
  paddingBottom: '10px',
  margin: '0px auto',
  fontSize: '30px'
};

const valueStyle = {
  marginLeft: '250px',
  color: 'blue',
  fontSize: '30px'
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)