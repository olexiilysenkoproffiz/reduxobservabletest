import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Dashboard from './Dashboard';

describe('<Dashboard />', () => {
    describe('render()', () => {
        test('renders the component', () => {
            const wrapper = shallow(<Dashboard />);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });
});


