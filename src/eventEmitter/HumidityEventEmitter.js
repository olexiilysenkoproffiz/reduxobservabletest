import EventEmitter from './EventEmitter';

export const humidity = "HUMIDITY";
export const combined = "COMBINED";

class HumidityEventEmitter {
    constructor() {
        this.eventEmiter = new EventEmitter();
        this._startEmiter();

        return this.eventEmiter;
    }

    _startEmiter() {
        const fakeDelay = (Math.floor(Math.random() * 100) + 100);
        setTimeout(() => {
            const fakeValue = (Math.floor(Math.random() * 90) + 10).toString();
            this.eventEmiter.emit(humidity, fakeValue);
            this._startEmiter();
        }, fakeDelay);
    }
}

export default new HumidityEventEmitter();