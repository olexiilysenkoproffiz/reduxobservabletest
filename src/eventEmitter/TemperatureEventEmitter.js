import EventEmitter from './EventEmitter';

export const temperature = "TEMPERATURE";


class TemperatureEventEmitter {
    constructor() {
        this.eventEmiter = new EventEmitter();
        this._startEmiter();

        return this.eventEmiter;
    }

    _startEmiter() {
        const fakeDelay = (Math.floor(Math.random() * 100) + 100);
        setTimeout(() => {
            const fakeValue = (Math.floor(Math.random() * 10) + 30).toString();
            this.eventEmiter.emit(temperature, fakeValue);
            this._startEmiter();
        }, fakeDelay);
    }
}

export default new TemperatureEventEmitter();