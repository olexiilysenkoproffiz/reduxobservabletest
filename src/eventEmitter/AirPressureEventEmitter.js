import EventEmitter from './EventEmitter';

export const airPressure = "AIR_PRESSURE";

class AirPressureEventEmitter {
  constructor() {
    this.eventEmiter = new EventEmitter();
    this._startEmiter();

    return this.eventEmiter;
  }

  _startEmiter() {
    const fakeDelay = (Math.floor(Math.random() * 100) + 100);
    setTimeout(() => {
      const fakeValue = (Math.floor(Math.random() * 300) + 100).toString();
      this.eventEmiter.emit(airPressure, fakeValue);
      this._startEmiter();
    }, fakeDelay);
  }
}

export default new AirPressureEventEmitter();