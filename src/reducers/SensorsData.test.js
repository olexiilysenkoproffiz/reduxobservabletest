import { sourceDataReducer } from './SensorsData';

describe('INITIAL_STATE', () => {
    test('is correct', () => {
        const action = { type: 'dummy_action' };
        const initialState = {
            temperature: 'N/A',
            airPressure: 'N/A',
            humidity: 'N/A',
            combined: 'N/A'
        };

        expect(sourceDataReducer(undefined, action)).toEqual(initialState);
    });
});

describe('COMBINED', () => {
    test('returns the correct state', () => {
        const payload = {
            temperature: 36,
            airPressure: 330,
            humidity: 98,
            combined: {
                temperature: 36,
                airPressure: 330,
                humidity: 98,
            },
        }
      let action = { type: "COMBINED", payload };
      const expectedState = { 
            temperature: 36,
            airPressure: 330,
            humidity: 98,
            combined: {
                temperature: 36,
                airPressure: 330,
                humidity: 98,
            }};
  
      expect(sourceDataReducer(undefined, action)).toEqual(expectedState);
    });
  });