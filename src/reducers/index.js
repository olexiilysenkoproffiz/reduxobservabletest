import { combineReducers } from 'redux';
import { sourceDataReducer } from './SensorsData'

export const rootReducer = combineReducers({
  sourceDataReducer,
});