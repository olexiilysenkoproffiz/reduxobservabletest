import { humidity, combined } from '../eventEmitter/HumidityEventEmitter';
import { airPressure } from '../eventEmitter/AirPressureEventEmitter';
import { temperature } from '../eventEmitter/TemperatureEventEmitter';

const initialState = {
  temperature: 'N/A',
  airPressure: 'N/A',
  humidity: 'N/A',
  combined: 'N/A'
};


export const sourceDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case humidity:
      return {
        ...state,
      };

    case airPressure:
      return {
        ...state,
      };

    case temperature:
      return {
        ...state,
      };

    case combined:
      return {
        ...state,
        temperature: action.payload.temperature,
        airPressure: action.payload.airPressure,
        humidity: action.payload.humidity,
        combined: action.payload.combined,
      };

    default:
      return {
        ...state
      };
  }
};