import { combineLatest } from 'rxjs';
import { humidity, combined } from '../eventEmitter/HumidityEventEmitter';
import { temperature } from '../eventEmitter/TemperatureEventEmitter';
import { airPressure } from '../eventEmitter/AirPressureEventEmitter';
import createSensorDataAction from '../actions/SensorData';
import {
  debounceTime,
  map
} from 'rxjs/operators';

export const sensorsDataEpic = action$ => {
  return (
    combineLatest(
      action$.ofType(airPressure),
      action$.ofType(temperature),
      action$.ofType(humidity),
    )
      .pipe(
        map((res) => createSensorDataAction(combined,
          {
            temperature: res[1].payload,
            airPressure: res[0].payload,
            humidity: res[2].payload,
          }
        )),
        debounceTime(100),
      )
  );
}

