import { combineEpics } from 'redux-observable';
import { sensorsDataEpic } from './SensorsDataEpic';

export const rootEpic = combineEpics(
  sensorsDataEpic,
);
