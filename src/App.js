import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import configureStore from './store';

import Dashboard from './components/Dashboard';

let store = configureStore();

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to Doctor`s dashboard!</h1>
            <p className="App-intro">
              A live analytics dashboard displays data about
              a medical operating room to doctors and nurses.
          </p>
          </header>
          <br />
          <br />
          <br />
          <Dashboard />
        </div>
      </Provider>
    );
  }
}

export default App;
