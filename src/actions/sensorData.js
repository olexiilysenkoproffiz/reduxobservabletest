
/*
 * action creators
 */
export default function createSensorDataAction(type, data) {
  return { type, payload: data }
}
