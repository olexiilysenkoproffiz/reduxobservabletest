import configureStore from 'redux-mock-store';
import createSensorDataAction from './SensorData';


const mockStore = configureStore();
const store = mockStore();

describe('select_actions', () => {
    beforeEach(() => {
        store.clearActions();
    });
});

describe('HUMIDITY', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedActions = [
        {
          'payload': 86,
          'type': 'HUMIDITY',
        },
      ];
  
      store.dispatch(createSensorDataAction("HUMIDITY", 86));
      expect(store.getActions()).toMatchSnapshot();
    });
  });

