
  
# cra-redux-observable


This project is an [Create React App ](https://github.com/facebookincubator/create-react-app) boilerplate with integration of Redux, and Redux-observable for quick starting your redux applications with the power of RxJs


Before starting with project, please headover to the [Create-React-App](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md
) documentation.





## Getting Started

1. Clone this repo

`https://bitbucket.org/olexiilysenkoproffiz/reduxobservabletest.git`

2. To run, go to project folder and run

`$ npm install`
or
`$ yarn install` (if you are using yarn)

3. Now start dev server by running -

`$ npm start`
or
`$ yarn start`

4. If the browser does not automatically for any reason visit - http://localhost:3000/

For more create-react-app commands refer `package.json`



### Redux-Observable Integration

The logic for your reducers (aka Redux logic) will all be contained with `/src/reducers/`. The logic for your epics (aka Redux-Observable) will all be contained in `/src/epics`.

#### Understand that every action will go to a Reducer before going into any Epic. 



## Recommended Resources

[Official Facebook Documentation on React](https://reactjs.org/docs/getting-started.html) 

[Official Redux Documentation](https://redux.js.org/)

[Official Redux-Observable Documentation](https://redux-observable.js.org/)

[Official RxJS Documentation](https://rxjs-dev.firebaseapp.com/)
